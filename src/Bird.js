import React from 'react'
import data from './bird.json'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

function Bird() {

    const displayData = data.sort((a, b) => a.finnish.localeCompare(b.finnish)).map(
        (data) => {
            return(
                <TableRow>
                    <TableCell>{data.finnish}</TableCell>
                    <TableCell>{data.swedish}</TableCell>
                    <TableCell>{data.english}</TableCell>
                    <TableCell>{data.short}</TableCell>
                    <TableCell>{data.latin}</TableCell>
                </TableRow>
            )
        }
    )

  return (
    <TableContainer component={Paper}>
        <Table>
            <TableHead>
                <TableRow style={{backgroundColor: "lightgray"}}>
                    <TableCell style={{fontWeight: "bold"}}>Finnish</TableCell>
                    <TableCell style={{fontWeight: "bold"}}>Swedish</TableCell>
                    <TableCell style={{fontWeight: "bold"}}>English</TableCell>
                    <TableCell style={{fontWeight: "bold"}}>Short</TableCell>
                    <TableCell style={{fontWeight: "bold"}}>Latin</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {displayData}
            </TableBody>
            </Table>
    </TableContainer>
  )
}

export default Bird